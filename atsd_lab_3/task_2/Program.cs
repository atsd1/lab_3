﻿using System;
using System.Diagnostics;

namespace task_2
{
    public class Program
    {
        public static decimal Factorial(decimal n)
        {
            if (n == 0) return 1;

            return n * Factorial(n - 1);
        }
        public static void SortArray(int[] arr)
        {
            int temp = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr.Length - 1; j++)
                {
                    if (arr[j] < arr[j + 1])
                    {
                        temp = arr[j + 1];
                        arr[j + 1] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            int n = 20;
            for (int i = 0; i <= n; i++)
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                decimal result = Factorial(i);
                stopwatch.Stop();

                Console.WriteLine($"Factorial({i}): {result}; Time: {stopwatch.Elapsed.TotalMilliseconds} ms.");
            }

            Random random = new Random();
            int[] array = new int[25];

            //Console.WriteLine("Unsorted");
            //for (int i = 0; i < array.Length; i++)
            //{
            //    array[i] = random.Next(0, 100);
            //    Console.WriteLine(array[i]); 
            //}

            Stopwatch stopwatchSort = Stopwatch.StartNew();
            SortArray(array);
            stopwatchSort.Stop();

            Console.WriteLine($"\nSorting Time: {stopwatchSort.Elapsed.TotalSeconds} s.");

            //Console.WriteLine("Sorted");
            //for (int i = 0; i < array.Length; i++)
            //{
            //    Console.WriteLine(array[i]);
            //}


            Console.ReadLine();
        }
    }
}
