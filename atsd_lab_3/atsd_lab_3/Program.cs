﻿using System;

namespace atsd_lab_3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            TabulationFunction("f(x) = x", 0, 50, 1, x => x);
            TabulationFunction("f(x) = log(x)", 0, 50, 1, x => (decimal)Math.Log((double)x));
            TabulationFunction("f(x) = x*log(x)", 0, 50, 1, x => x*(decimal)Math.Log((double)x));
            TabulationFunction("f(x) = x^2", 0, 50, 1, x => (decimal)Math.Pow((double)x, 2));
            TabulationFunction("f(x) = 2^x", 0, 50, 1, x => (decimal)Math.Pow(2, (double)x));
            TabulationFunction("f(x) = x!", 0, 50, 1, task_2.Program.Factorial);

            Console.ReadLine();
        }
        public static void TabulationFunction(string title, decimal from, decimal to, decimal iterationStep, Func<decimal, decimal> func)
        {
            const int bufferSize = -10;
            Console.WriteLine(title);
            Console.WriteLine($"{'X', bufferSize}{'Y', bufferSize}");
            for (decimal i = from; i <= to; i += iterationStep)
            {
                string temp;
                try
                {
                    temp = func(i).ToString();
                }
                catch (Exception ex)
                {
                    temp = ex.Message;
                }

                Console.WriteLine($"{i,bufferSize}{temp,bufferSize}");
            }

            Console.WriteLine();
        }

    }
}
